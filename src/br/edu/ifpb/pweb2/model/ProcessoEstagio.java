package br.edu.ifpb.pweb2.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class ProcessoEstagio {

    @Id
    private Integer numero;

    @OneToOne
    private Aluno aluno;
    
    private Date data1;
    private Date data2;
    private Date data3;
    private Date data4;
    private Date data5;
    private Date data6;
    
    private String descricao1;
    private String descricao2;
    private String descricao3;
    private String descricao4;
    private String descricao5;
    private String descricao6;
    
    
    public String getDescricao1() {
		return descricao1;
	}

	public void setDescricao1(String descricao1) {
		this.descricao1 = descricao1;
	}

	public String getDescricao2() {
		return descricao2;
	}

	public void setDescricao2(String descricao2) {
		this.descricao2 = descricao2;
	}

	public String getDescricao3() {
		return descricao3;
	}

	public void setDescricao3(String descricao3) {
		this.descricao3 = descricao3;
	}

	public String getDescricao4() {
		return descricao4;
	}

	public void setDescricao4(String descricao4) {
		this.descricao4 = descricao4;
	}

	public String getDescricao5() {
		return descricao5;
	}

	public void setDescricao5(String descricao5) {
		this.descricao5 = descricao5;
	}

	public String getDescricao6() {
		return descricao6;
	}

	public void setDescricao6(String descricao6) {
		this.descricao6 = descricao6;
	}

	public Date getData1() {
		return data1;
	}

	public void setData1(Date data1) {
		this.data1 = data1;
	}

	public Date getData2() {
		return data2;
	}

	public void setData2(Date data2) {
		this.data2 = data2;
	}

	public Date getData3() {
		return data3;
	}

	public void setData3(Date data3) {
		this.data3 = data3;
	}

	public Date getData4() {
		return data4;
	}

	public void setData4(Date data4) {
		this.data4 = data4;
	}

	public Date getData5() {
		return data5;
	}

	public void setData5(Date data5) {
		this.data5 = data5;
	}

	public Date getData6() {
		return data6;
	}

	public void setData6(Date data6) {
		this.data6 = data6;
	}

	public ProcessoEstagio() {}

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }
    

}
