package br.edu.ifpb.pweb2.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Aluno implements Serializable{

  
	private static final long serialVersionUID = 1L;

	@Id
    private Long matricula;

    private String nome;
    private String email;
    private String telefone;
    private String tituloTrabalho;
    
    private Double nota;

    @OneToOne
    private Professor orientador;
    
    @OneToOne
    private Professor professor1;
    
    @OneToOne
    private Professor professor2;
    
    @OneToOne
    private Professor professor3;
   
    
    @OneToOne(mappedBy="aluno",optional=true)
    private ProcessoEstagio estagio;
    
    @OneToOne(mappedBy="aluno",optional=true)
    private ProcessoConclusao colacao;

    public Aluno() {
    }

    public Long getMatricula() {
        return matricula;
    }

    public void setMatricula(Long matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

	public Double getNota() {
		return nota;
	}

	public void setNota(Double nota) {
		this.nota = nota;
	}

	public Professor getOrientador() {
		return orientador;
	}

	public void setOrientador(Professor orientador) {
		this.orientador = orientador;
	}


	

    public ProcessoEstagio getEstagio() {
		return estagio;
	}

	public void setEstagio(ProcessoEstagio estagio) {
		this.estagio = estagio;
	}

	public ProcessoConclusao getColacao() {
		return colacao;
	}

	public void setColacao(ProcessoConclusao colacao) {
		this.colacao = colacao;
	}

	public String getTituloTrabalho() {
        return tituloTrabalho;
    }

    public void setTituloTrabalho(String tituloTrabalho) {
        this.tituloTrabalho = tituloTrabalho;
    }

	public Professor getProfessor1() {
		return professor1;
	}

	public void setProfessor1(Professor professor1) {
		this.professor1 = professor1;
	}

	public Professor getProfessor2() {
		return professor2;
	}

	public void setProfessor2(Professor professor2) {
		this.professor2 = professor2;
	}

	public Professor getProfessor3() {
		return professor3;
	}

	public void setProfessor3(Professor professor3) {
		this.professor3 = professor3;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", email=" + email + ", telefone=" + telefone
				+ ", tituloTrabalho=" + tituloTrabalho + ", nota=" + nota + ", orientador=" + orientador
				+ ", professor1=" + professor1 + ", professor2=" + professor2 + ", professor3=" + professor3
				+ ", estagio=" + estagio + ", colacao=" + colacao + "]";
	}

	
        
	
    
}
