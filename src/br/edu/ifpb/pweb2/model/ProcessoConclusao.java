package br.edu.ifpb.pweb2.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class ProcessoConclusao {

    @Id
    private Integer numero;

    @OneToOne
    private Aluno aluno;
    
    private Date colacao;
    private String colacaoDescricao;
    
    private Date finalizado;
    private String finalDescricao;
    
    private boolean enade;
    private boolean semestre;
    private boolean cargaHoraria;
    private boolean atividadesComplementares;
    
	public ProcessoConclusao() {}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public Date getColacao() {
		return colacao;
	}

	public void setColacao(Date colacao) {
		this.colacao = colacao;
	}

	public String getColacaoDescricao() {
		return colacaoDescricao;
	}

	public void setColacaoDescricao(String colacaoDescricao) {
		this.colacaoDescricao = colacaoDescricao;
	}

	public String getFinalDescricao() {
		return finalDescricao;
	}

	public void setFinalDescricao(String finalDescricao) {
		this.finalDescricao = finalDescricao;
	}

	public boolean isEnade() {
		return enade;
	}

	public void setEnade(boolean enade) {
		this.enade = enade;
	}

	public boolean isSemestre() {
		return semestre;
	}

	public void setSemestre(boolean semestre) {
		this.semestre = semestre;
	}

	public boolean isCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(boolean cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	public boolean isAtividadesComplementares() {
		return atividadesComplementares;
	}

	public void setAtividadesComplementares(boolean atividadesComplementares) {
		this.atividadesComplementares = atividadesComplementares;
	}

	public Date getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Date finalizado) {
		this.finalizado = finalizado;
	}

    

}
