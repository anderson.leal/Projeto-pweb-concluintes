package br.edu.ifpb.pweb2.dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.edu.ifpb.pweb2.model.ProcessoEstagio;

public class ProcessoEstagioDAO extends DAO<Integer,ProcessoEstagio>{

	@SuppressWarnings("unchecked")
	public List<ProcessoEstagio> getAlunosProtocolo() {
		try {
			Query q = super.entityManager.createQuery(
					"Select p from ProcessoEstagio p where p.data1 is null");
			return (List<ProcessoEstagio>) q.getResultList();
		} catch (NoResultException e) {
			return null;
		}

	}
	
	@SuppressWarnings("unchecked")
	public List<ProcessoEstagio> getAlunosCoordenacao() {
		try {
			Query q = super.entityManager.createQuery(
					"Select p from ProcessoEstagio p where p.data2 is null and p.data1 is not null");
			return (List<ProcessoEstagio>) q.getResultList();
		} catch (NoResultException e) {
			return null;
		}

	}
	@SuppressWarnings("unchecked")
	public List<ProcessoEstagio> getAlunosOrientador() {
		try {
			Query q = super.entityManager.createQuery(
					"Select p from ProcessoEstagio p where p.data3 is null  and p.data2 is not null");
			return (List<ProcessoEstagio>) q.getResultList();
		} catch (NoResultException e) {
			return null;
		}

	}
	@SuppressWarnings("unchecked")
	public List<ProcessoEstagio> getAlunosRetornoCoordenacao() {
		try {
			Query q = super.entityManager.createQuery(
					"Select p from ProcessoEstagio p where p.data4 is null  and p.data3 is not null");
			return (List<ProcessoEstagio>) q.getResultList();
		} catch (NoResultException e) {
			return null;
		}

	}
	
	@SuppressWarnings("unchecked")
	public List<ProcessoEstagio> getAlunosCoordenacaoEstagio() {
		try {
			Query q = super.entityManager.createQuery(
					"Select p from ProcessoEstagio p where p.data5 is null and p.data4 is not null");
			return (List<ProcessoEstagio>) q.getResultList();
		} catch (NoResultException e) {
			return null;
		}

	}
	
	@SuppressWarnings("unchecked")
	public List<ProcessoEstagio> getAlunosFinalizadoProcesso() {
		try {
			Query q = super.entityManager.createQuery(
					"Select p from ProcessoEstagio p where p.data6 is null and p.data5 is not null");
			return (List<ProcessoEstagio>) q.getResultList();
		} catch (NoResultException e) {
			return null;
		}

	}
	
	@SuppressWarnings("unchecked")
	public List<ProcessoEstagio> getAlunosConclusao() {
		try {
			Query q = super.entityManager.createQuery(
					"Select p from ProcessoEstagio p where p.data1 is not null and p.data2 is not null and p.data3 is not null and p.data4 is not null and p.data5 is not null and p.data6 is not null");
			return (List<ProcessoEstagio>) q.getResultList();
		} catch (NoResultException e) {
			return null;
		}

	}
	
}
