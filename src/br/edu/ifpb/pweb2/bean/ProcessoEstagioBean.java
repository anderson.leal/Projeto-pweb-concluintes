/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.pweb2.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import br.edu.ifpb.pweb2.dao.ProcessoEstagioDAO;
import br.edu.ifpb.pweb2.model.ProcessoEstagio;

/**
 *
 * @author andersonleal
 */
@ManagedBean
@ViewScoped
public class ProcessoEstagioBean extends GenericBean{
	
    private ProcessoEstagioDAO dao;
    private ProcessoEstagio processo;
    private List<ProcessoEstagio> processos;
    private Integer filtro;
    private boolean editar;
    
    public ProcessoEstagio getProcesso() {
		return processo;
	}

	public void setProcesso(ProcessoEstagio processo) {
		this.processo = processo;
	}

	public List<ProcessoEstagio> getProcessos() {
		return processos;
	}

	public void setProcessos(List<ProcessoEstagio> processos) {
		this.processos = processos;
	}

    @PostConstruct
    private void init() {
        dao = new ProcessoEstagioDAO();
        processo = new ProcessoEstagio();
        editar = false;
        this.protocolo();
    }
       
    public void protocolo(){
    	processos = dao.getAlunosProtocolo();
    }
    
    public void coordenacao(){
    	processos = dao.getAlunosCoordenacao();
    }
    
    public void orientador(){
    	processos = dao.getAlunosOrientador();
    }

    public void retornoCoordenacao(){
    	processos = dao.getAlunosRetornoCoordenacao();
    }
    
    public void coordenacaoEstagio(){
    	processos = dao.getAlunosCoordenacaoEstagio();
    }
    
    public void finalizadoProcesso(){
    	processos = dao.getAlunosFinalizadoProcesso();
    }
    public void loadProcesso() {
    	if(processo.getNumero()!=null)
    		processo = dao.getById(processo.getNumero());
    }
    
    public void loadTodos() {
        setProcessos(dao.findAll());
    }
    
    public void filtrar(ActionEvent e){
    	switch(filtro){
    	case 0:
    		this.loadTodos();
    		break;
    	case 1:
    		this.protocolo();
    		break;
    	case 2:
    		this.coordenacao();
    		break;
    	case 3:
    		this.orientador();
    		break;
    	case 4:
    		this.retornoCoordenacao();
    		break;
    	case 5:
    		this.coordenacaoEstagio();
    		break;
    	case 6:
    		this.finalizadoProcesso();
    		break;
    	}
    }
    
    public String editar() {
        editar = true;
        return "lancamento_estagio?faces-redirect=true&numero="+processo.getNumero();
    }
       
    public void apagar(ActionEvent event) {
        dao.beginTransaction();
        dao.delete(processo);
        dao.commit();
        setProcessos(dao.findAll());
    }
    
    public void salvar(ActionEvent event) {
        
        try {
            dao.beginTransaction();
            if(editar)
            	dao.update(processo);
            else
            	dao.save(processo);
            dao.commit();
            addMessage("Cadastrado com sucesso.");
        } catch (Exception e) {
            addError("Error ao salvar.");
        }
        
    }

	public Integer getFiltro() {
		return filtro;
	}

	public void setFiltro(Integer filtro) {
		this.filtro = filtro;
	}

	
}
