/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.pweb2.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

import org.primefaces.event.RowEditEvent;

import br.edu.ifpb.pweb2.dao.ProfessorDAO;
import br.edu.ifpb.pweb2.model.Professor;

/**
 *
 * @author andersonleal
 */
@ManagedBean
public class ProfessorBean extends GenericBean {
    
    private ProfessorDAO dao;
    private Professor professor;
    private List<Professor> professores;
    
    @PostConstruct
    private void init() {
        dao = new ProfessorDAO();
        professor = new Professor();
        professores = dao.findAll();
    }
    //Colocar no beanGenerico
    public void atualizaLista() {
        setProfessores(dao.findAll());
    }
    
    public void onRowEdit(RowEditEvent event) {
        try {
            dao.beginTransaction();
            dao.update((Professor) event.getObject());
            dao.commit();
            this.atualizaLista();
            addMessage("Atualizado com sucesso.");            
        } catch (Exception e) {
            this.addError("Erro ao atualizar");
        }
        
    }
    
    public void apagar(ActionEvent event) {
        try {
            dao.beginTransaction();
            dao.delete(professor);
           	dao.commit();
            this.atualizaLista();
        } catch (Exception e) {
            this.addError("Erro ao apagar!");
        }
        
    }
    
    public void salvar(ActionEvent event) {
        
        try {
            dao.beginTransaction();
            dao.save(professor);
            dao.commit();
            addMessage("Cadastrado com sucesso.");
            this.atualizaLista();
        } catch (Exception e) {
            addError("Error ao salvar.");
        }
        
    }
    
    public Professor getProfessor() {
        return professor;
    }
    
    public void setProfessor(Professor professor) {
        this.professor = professor;
    }
    
    public List<Professor> getProfessores() {
        return professores;
    }
    
    public void setProfessores(List<Professor> professores) {
        this.professores = professores;
    }
}
