package br.edu.ifpb.pweb2.util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import br.edu.ifpb.pweb2.dao.ProfessorDAO;
import br.edu.ifpb.pweb2.model.Professor;
//import br.edu.ifpb.pweb2.service.ProfessorService;

@FacesConverter("professorConverter")
public class ProfessorConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String matricula) {
		
	if(matricula != null && matricula.trim().length() > 0) {
            try {
            	ProfessorDAO dao = new ProfessorDAO();
            	return dao.getById(matricula);
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        }
        
		return null;
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
			if(object != null) {
	            return String.valueOf(((Professor) object).getMatricula());
	        }
	 
            return null;
	}

}
